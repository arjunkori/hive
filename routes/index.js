var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	console.log("####### RENDER DASH BOARD #########");
  res.render('index', { message: 'Express' });
});


router.get('/connect', function(req, res) {
    console.log("### INSIDE CONNECTION TO HIVE ###");
    var hive = require('thrift-hive');
    var thrift = require('thrift');
    transport = thrift.TBufferedTransport();
    protocol = thrift.TBinaryProtocol();

    var client = hive.createClient({
        version: '0.7.1-cdh3u2',
        server: '10.0.66.240',
        port: 9083,
        timeout: 1000
    });
    // Execute call 
    client.execute('use default', function(err) {
        client.query('show tables')
            .on('row', function(database) {
                console.log("########### CONNECTED SUCCESSFULL #####");
                console.log(database);
            })
            .on('error', function(err) {
                console.log("######### ERROR ########## "+err)
                console.log(err.message);
                client.end();
            }).on('end', function() {
                console.log('########## END ###########')
                client.end();
        });
    });
});

router.get('/connect2', function(req, res) {
    console.log("######## connect2 ########## ");

    var thrift = require('thrift');
    transport = thrift.TBufferedTransport();
    protocol = thrift.TBinaryProtocol();

    var hive = require('node-hive').for({
        server: "hive.myserver"
    });

    hive.fetch("SELECT * FROM my_table", function(err, data) {
        if (err) {
            console.log("######## RROR ###########")
        } else {
            data.each(function(record) {
                console.log(record);
            });
        }
    });
})
module.exports = router;
