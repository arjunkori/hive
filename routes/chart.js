var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	console.log("####### INSIDE CHSRT JS #########");

	/*var data={
		percentages:'45',
		closeness:'45',
		curiosity:'45',
		excitement:'77',
		harmony:'96',
		liberty:'43'
	}*/

  res.render('chart', {
		percentages:'45',
		closeness:'45',
		curiosity:'45',
		excitement:'77',
		harmony:'96',
		liberty:'43'
	});
});

router.get('/line',function(req,res){
	console.log("######### LINE ###########");

	res.render('chartLine',{data:''})

})

module.exports = router;
